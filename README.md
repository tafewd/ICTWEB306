# ICTWEB306 Develop web presence using social media

## Assignment 2

### Example project

- **Business name**: Vladimir's Pet Store
- **Business type**: Store with a physical address
- **Business goal**: To promote pet food / pet toys online to attaract more customers online and in store.

### Client requirements

1. Add disclaimer to sicial media accounts.

```
This soicial media account is created for academic purposes only and should be ignored after December 2023.
```

2. Add link to business URL: 

- https://gitlab.com/tafewd/ICTWEB306/
